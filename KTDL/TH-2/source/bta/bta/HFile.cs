﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bta
{
    class HFile
    {
        public static int GetStringFile(string path)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine("File khong ton tai");
            }
            int counter = 0;
            string line;
            Dictionary<string, int> groups = new Dictionary<string, int>();

            System.IO.StreamReader file = new System.IO.StreamReader(path);
            while ((line = file.ReadLine()) != null)
            {
                //Console.WriteLine(line);
                if (groups.ContainsKey(line))
                {
                    groups[line]++;
                }
                else
                {
                    groups.Add(line, 1);
                }
                counter++;
            }

            file.Close();

            return counter;
        }

    }
    class Group
    {

        public Dictionary<string, string[]> rows;
        public List<string> columns;

        public Group(string path)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine("File khong ton tai");
            }
            int counter = 0;
            string line;
            Dictionary<string, string[]> groups = new Dictionary<string, string[]>();
            List<string> columns = new List<string>();
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            while ((line = file.ReadLine()) != null)
            {
                //Console.WriteLine(line);
                string[] splits = line.Split(',');
                string keyName = splits[0];
                string[] properties = splits.Skip(1).Take(splits.Length).ToArray();
                foreach (string item in properties)
                {
                    if (!columns.Contains(item))
                    {
                        columns.Add(item);
                    }
                }
                if (!groups.ContainsKey(keyName))
                {
                    groups[keyName] = properties;
                }

                counter++;
            }

            file.Close();
            this.rows = groups;
            this.columns = columns;
        }


        public void progess()
        {
            Dictionary<string, string[]> rows = new Dictionary<string, string[]>();
            List<string> els = new List<string>();
            foreach (var pair in this.rows)
            {
                for (int i = 0; i < this.columns.Count; i++)
                {
                    if (pair.Value.Contains(this.columns[i]))
                    {
                        els.Add("yes");
                    }
                    else
                    {
                        els.Add("?");
                    }
                }
                rows.Add(pair.Key, els.ToArray());
                els.Clear();
            }
            this.rows = rows;
        }

        public bool writeFile(string path)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(path);
            string nameCololums = "name";
            foreach (string column in this.columns)
            {
                nameCololums += "," + column;
            }
            file.WriteLine(nameCololums);
            foreach (var pair in this.rows)
            {
                string els = pair.Key;
                foreach (string item in pair.Value)
                {
                    els += "," + item;
                }
                file.WriteLine(els);
            }
            file.Close();
            return true;
        }
    }


}
